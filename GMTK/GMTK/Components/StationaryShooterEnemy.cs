﻿using Nurose.Core.System;
using Nurose.Core.Types;

namespace GMTK
{
    public class StationaryShooterEnemy : Component
    {
        private ShootComponent ShootComponent;
        public Vector2 Direction;

        public override void Start()
        {
            ShootComponent = Entity.GetComponent<ShootComponent>();
            
        }

        public void Step()
        {
            ShootComponent.Shoot(Transform.GlobalPosition + Direction, Direction);
        }

        public void Update()
        {
        }
    }
}

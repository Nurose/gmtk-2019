﻿using Nurose.Core.System;
using Nurose.Core.Types;
using Nurose.Core.Graphics;

namespace GMTK
{
    public class MoveIndicator : Component
    {
        public UnitRectangleDrawTask drawTask;
        private CharacterMovement playerMovement;
        private Player player;
        private ActionSystem ActionSystem;
        private Vector2 IndicatorDirection;
        private Texture Texture;

        public override void Start()
        {
            Texture = ResourceManager["whiteTexture"] as Texture;
            drawTask = new UnitRectangleDrawTask(new Color(1,1,1,.3f), 1);
            player = Transform.Parent.Entity.GetComponent<Player>();
            playerMovement = Transform.Parent.Entity.GetComponent<CharacterMovement>();
            ActionSystem = World.GetSystem<ActionSystem>();
        }


        public void Draw()
        {
            if (!ActionSystem.LongPressing && player.CurrentAction is TurnAction ta )
            {
                IndicatorDirection = ta.Direction;
                Transform.GlobalPosition = playerMovement.RealGridPos + IndicatorDirection;
                
                Drawer.SetTexture(Texture);
                Drawer.SetModelTransform(this.Transform);
                Drawer.Draw(drawTask);
            }
        }
    }
}

﻿using Nurose.Core.System;

namespace GMTK
{
    public class Key : Component, ICollide
    {
        LevelLoadingSystem LevelLoadingSystem;

        public override void Start()
        {
            LevelLoadingSystem = World.GetSystem<LevelLoadingSystem>();
        }

        public void OnCollide(CharacterMovement other)
        {
            this.Transform.Children[0].Entity.Destroy();
            this.Entity.Destroy();
            LevelLoadingSystem.CurrentLevel.OpenDoor();
        }
    }
}

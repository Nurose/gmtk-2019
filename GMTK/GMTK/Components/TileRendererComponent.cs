﻿using Nurose.Core.System;
using Nurose.Core.Graphics;
using Nurose.Core.Types;

namespace GMTK
{
    public class TileRendererComponent : Component
    {
        public const int TextureUnitSize = 16;
        public static Vector2 TileOffset { get; private set; }
        public GridTask GridTask;
        public Vector2 BlockSize = new Vector2(1, 1);
        private LevelLoadingSystem levelLoadingSystem;

        public override void Start()
        {
            levelLoadingSystem = World.GetSystem<LevelLoadingSystem>();
            Texture texture = ResourceManager["tilesheet"] as Texture;
            World.RenderTarget.SetTexture(texture);
            GridTask = new GridTask(levelLoadingSystem.CurrentLevel, texture, TextureUnitSize);
            levelLoadingSystem.OnLevelSwitch += LevelLoadingSystem_OnLevelSwitch;
        }

        private void LevelLoadingSystem_OnLevelSwitch(object sender, Level e)
        {
            GridTask.Level = levelLoadingSystem.CurrentLevel;
            GridTask.Refresh();
        }

        // TODO is debug
        public void Update()
        {
            if (Input.GetKeyReleased(Nurose.Core.Window.Key.F1))
            {
                levelLoadingSystem.PreviousLevel();
                GridTask.Level = levelLoadingSystem.CurrentLevel;
                GridTask.Refresh();
            }

            if (Input.GetKeyReleased(Nurose.Core.Window.Key.F2))
            {
                levelLoadingSystem.NextLevel();
                //GridTask.Level = levelLoadingSystem.CurrentLevel;
               // GridTask.Refresh();
            }
        }

        public void Draw()
        {
            var cam = World.GetAllComponentsOfType<Camera>()[0];
            int div = (int)cam.Transform.LocalSize.X * 2;
            TileOffset = new Vector2(-World.RenderTarget.Size.X, World.RenderTarget.Size.Y) / div;
            Drawer.SetModelTransform(new Transform()
            {
                LocalSize = new Vector2(BlockSize.X, -BlockSize.Y),
                GlobalPosition = TileOffset
            });
            Drawer.Draw(GridTask);
        }
    }
}
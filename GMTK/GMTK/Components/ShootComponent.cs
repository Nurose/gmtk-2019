﻿using Nurose.Core.Graphics;
using Nurose.Core.System;
using Nurose.Core.Types;
using System;

namespace GMTK
{
   
    public class ShootComponent : Component
    {

        float StepsBetweenShots =2;
        ActionSystem ActionSystem;
        int stepsSinceLastShot;


        public override void Start()
        {
            ActionSystem = World.GetSystem<ActionSystem>();
        }

        
        public void Step()
        {
            stepsSinceLastShot++;
        }


        public void Shoot(Vector2 pos, Vector2 dir) 
        {
            if(stepsSinceLastShot >= StepsBetweenShots)
            {
                var bullet = new Entity();
                bullet.Transform.GlobalPosition = pos;
                World.AddEntity(bullet);
                var movement = bullet.AddComponent<CharacterMovement>();
                movement.DestroyOnTileHit = true;
                movement.Direction = dir;
                var textr = bullet.AddComponent<FourTextureRenderer>();
                textr.Texture = ResourceManager["bulletTexture"] as Texture;
                textr.ChangeDirection(dir);
                stepsSinceLastShot = 0;
            }
        }
    }
}

﻿using Nurose.Core.System;
using Nurose.Core.Types;
using System.Linq;

namespace GMTK
{
    public class Teleporter : Component, ICollide
    {

        public Vector2 TelePos;
        public int TeleId = 0;
        private Teleporter OtherTeleporter;

        public override void Start()
        {
            OtherTeleporter = World.GetAllComponentsOfType<Teleporter>().Single(t => t.TeleId == TeleId && t!=this);
        }



        public void OnCollide(CharacterMovement other)
        {
            Logger.LogAnnouncement("MOVe");
            other.RealGridPos = OtherTeleporter.Entity.Transform.GlobalPosition;
        }
        
    }
}

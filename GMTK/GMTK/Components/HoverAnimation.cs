﻿using Nurose.Core.System;
using Nurose.Core.Types;
using Nurose.Core.Window;
using System;

namespace GMTK
{
    public class HoverAnimation : Component
    {
        public float Speed = 4;
        public float Intensity = .05f;
        private float time;

        public void Update()
        {
            time += NuroseMain.Window.TimeSinceLastDrawCall();
            Transform.LocalPosition = new Vector2(0, (float)Math.Sin(time * Speed) * Intensity);
        }
    }
}

﻿using Nurose.Core.Graphics;

namespace GMTK
{


    public interface ICollide
    {
        void OnCollide(CharacterMovement other);
    }
}

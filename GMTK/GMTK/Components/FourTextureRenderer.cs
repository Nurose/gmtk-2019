﻿using Nurose.Core.Graphics;
using Nurose.Core.System;
using Nurose.Core.Types;

namespace GMTK
{
    public class FourTextureRenderer : Component
    {
        public Texture Texture;

        private VertexBuffer VertexBuffer;

        private Vector2 offset;
        private TurnAction lastTurnAction;
        private bool directionWasSet;
        public override void Start()
        {
            if (!directionWasSet)
                ChangeDirection(Vector2.Right);
        }

        private void FourTextureRenderer_OnSwitchEvent(object sender, Action e)
        {
            if (e is TurnAction turnAction)
            {
                lastTurnAction = turnAction;
                ChangeDirection(turnAction.Direction);
            }
        }

        public void ChangeDirection(Vector2 dir)
        {
            directionWasSet = true;
            var verts = new Vertex[4];
            if (dir == Vector2.Down)
            {
                offset = new Vector2(0f, 0f);
            }
            else if (dir == Vector2.Right)
            {
                offset = new Vector2(.5f, 0);
            }
            else if (dir == Vector2.Up)
            {
                offset = new Vector2(.5f, .5f);
            }
            else if (dir == Vector2.Left)
            {
                offset = new Vector2(0, .5f);
            }

            verts[0] = new Vertex(new Vector2(0,0), Color.White, offset);
            verts[1] = new Vertex(new Vector2(1, 0), Color.White, offset + new Vector2(.5f, 0f));
            verts[2] = new Vertex(new Vector2(1, 1), Color.White, offset + new Vector2(.5f, .5f));
            verts[3] = new Vertex(new Vector2(0, 1), Color.White, offset + new Vector2(0f, .5f));
            VertexBuffer = new VertexBuffer(verts);
        }



        public void Draw()
        {
            Drawer.SetTexture(Texture);
            Drawer.SetModelTransform(Transform);
            Drawer.Draw(VertexBuffer, PrimitiveType.Quads);
        }
    }
}

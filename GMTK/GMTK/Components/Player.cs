﻿using Nurose.Core.System;
using Nurose.Core.Types;

namespace GMTK
{
    public class Player : Component 
    {
        private CharacterMovement Movement;
        private FourTextureRenderer FourTextureRenderer;
        private ActionSystem ActionSystem;
        private ShootComponent ShootComponent;
        private Vector2 Direction;
        public Action CurrentAction;

        public override void Start()
        {
            ActionSystem = World.GetSystem<ActionSystem>();
            ActionSystem.OnSwitchEvent += ActionSystem_OnSwitchEvent;
            ActionSystem.OnStartLongPress += ActionSystem_OnStartLongPress;
            ShootComponent = Entity.GetComponent<ShootComponent>();
            Movement = Entity.GetComponent<CharacterMovement>();
            FourTextureRenderer = Entity.GetComponent<FourTextureRenderer>();
            Direction = Vector2.Right;
        }

        private void ActionSystem_OnStartLongPress(object sender, Action e)
        {
            if (e is TurnAction turnAction)
            {
                Movement.Fixed = false;
                Direction = turnAction.Direction;
                //FourTextureRenderer.ChangeDirection(turnAction.Direction);
                Movement.Direction = Direction;
            } else
            {
                Movement.Fixed = true;
            }
        }

        public void Step()
        {
            FourTextureRenderer.ChangeDirection(Direction);
            if (CurrentAction is ShootAction shoot)
            {
                ShootComponent.Shoot(Movement.RealGridPos + Direction, Direction);
            }
        }

        private void ActionSystem_OnSwitchEvent(object sender, Action e)
        {
            CurrentAction = e;

        }

        public void Update()
        {

        }
    }
}

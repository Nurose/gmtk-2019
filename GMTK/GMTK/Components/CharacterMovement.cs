﻿using Nurose.Core.System;
using Nurose.Core.Types;
using Nurose.Core.Utilities;
using System;

namespace GMTK
{
    public class CharacterMovement : Component, ICollide
    {
        public Vector2 Direction;
        public Vector2 LastStepDirection;
        public Vector2 RealGridPos;
        private CollisionSystem CollisionSystem;
        private LevelLoadingSystem LevelLoadingSystem;
        public bool DestroyOnTileHit = false;
        public bool IsTrigger = false;
        public bool Fixed;
        public override void Start()
        {
            RealGridPos = new Vector2((int)Math.Round(Transform.GlobalPosition.X), (int)Math.Round(Transform.GlobalPosition.Y));
            CollisionSystem = World.GetSystem<CollisionSystem>();
            LevelLoadingSystem = World.GetSystem<LevelLoadingSystem>();
            LastStepDirection = Direction;
        }

        private TileType? GetTile(Vector2 pos)
        {
            Level lvl = LevelLoadingSystem.CurrentLevel;
            return lvl.GetTile(lvl.WorldToGrid(RealGridPos + Direction));
        }

        public void Step()
        {
            if (Fixed)
            {
                return;
            }
            if (LastStepDirection == Direction)
            {
                var tile = GetTile(RealGridPos + Direction);
                if (tile.HasValue && tile.Value.Name == "13 OpenDoor")
                {
                    CollideWithDoor();
                }

                bool canMove = CollisionSystem.CanMove(RealGridPos + Direction);
                if (canMove)
                {

                    RealGridPos += Direction;

                }
                else if (DestroyOnTileHit)
                {
                    Entity.Destroy();
                }
            }
            LastStepDirection = Direction;
        }

        private void CollideWithDoor()
        {
            LevelLoadingSystem.NextLevel();
            Logger.LogAnnouncement("Next level", this);
        }

        public void Update()
        {
            Transform.GlobalPosition = Utils.Lerp(Transform.GlobalPosition, RealGridPos, Time.DeltaTime * 10);
        }

        public void OnCollide(CharacterMovement other)
        {
            if (!other.IsTrigger && !IsTrigger)
            {
                LevelLoadingSystem.ReloadLevel();
            }
        }
    }
}

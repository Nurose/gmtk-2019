﻿using Nurose.Core.Types;

namespace GMTK
{
    public struct TileType
    {
        public string Name { get; }
        public int TileSheetX { get; }
        public int TileSheetY { get; }
        public bool Collides { get; }
        public bool Kills { get; }

        public TileType(string name, int tileSheetX, int tileSheetY, bool collides, bool kills)
        {
            Name = name;
            TileSheetX = tileSheetX;
            TileSheetY = tileSheetY;
            Collides = collides;
            Kills = kills;
        }

        public Vector2 GetUV(int tileSize, Vector2 textureSize)
        {
            return new Vector2(TileSheetX * tileSize / textureSize.X, TileSheetY * tileSize / textureSize.Y);
        }

        public static readonly TileType[] Types = {
            new TileType("0 Air", 80/16, 64/16, true, false),
            new TileType("1 Floor", 48/16, 64/16, false, false),
            new TileType("2 Door", 48/16, 48/16, true, false),

            new TileType("3 PlatformTopLeft", 0, 0, false, false),
            new TileType("4 PlatformTop", 1, 0, false, false),
            new TileType("5 PlatformTopRight", 2, 0, false, false),

            new TileType("6 PlatformBottomLeft", 0, 1, false, false),
            new TileType("7 PlatformBottom", 1, 1, false, false),
            new TileType("8 PlatformBottomRight", 2, 1, false, false),

            new TileType("9 Cliff", 0, 2, true, false),

            new TileType("10 Left", 64/16, 64/16, false, false),
            new TileType("11 Right", 96/16, 64/16, false, false),
            new TileType("12 Transition", 80/16, 48/16, true, false),

            new TileType("13 OpenDoor", 80/16, 16/16, true, false),
        };
    }
}
﻿using Nurose.Core.Types;
using System;

namespace GMTK
{
    public struct Level
    {
        public int Width;
        public int Height;
        public readonly TileType[,] Tiles;
        public readonly EntityInstructions[] Entities;
        TileRendererComponent TileRenderer;
        public Level(int width, int height, TileType[,] tiles, EntityInstructions[] entities, TileRendererComponent tileRenderer)
        {
            Width = width;
            Height = height;
            Tiles = tiles;
            Entities = entities;
            TileRenderer = tileRenderer;
        }

        public static Vector2 GridToWorld(int x, int y, int levelWidth, int levelHeight)
        {
            return new Vector2(x - levelWidth / 2, levelHeight - y - levelHeight / 2 - 1);
        }

        public void OpenDoor()
        {
            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    if (Tiles[x, y].Name == "2 Door")
                    {
                        Tiles[x, y] = TileType.Types[13];
                        TileRenderer.GridTask.Refresh();
                    }
                }
            }
        }

        public TileType? GetTile(Vector2 p)
        {
            if (p.X >= Width || p.X < 0 || p.Y >= Height || p.Y < 0)
            {
                return null;
            }
            return Tiles[(int)p.X, (int)p.Y];
        }

        public Vector2 WorldToGrid(Vector2 v)
        {
            // x = lh -y- 0.5lh -1
            // x - lh = -y - lh/2 + 1 
            // x - lh  + lh/2 = -y + 1
            // x- .5lh - 1 = -y
            // -x + .5l + 1 = y

            return new Vector2((float)Math.Round(v.X) + Width / 2, (float)Math.Round (-v.Y) + 0.5f * Height - 1);
        }
    }
}

﻿using Nurose.Core.Graphics;
using Nurose.Core.System;

namespace GMTK
{
    public sealed class KeyInstructions : EntityInstructions
    {
        public KeyInstructions() : base("Key")
        {
        }

        public override Entity[] Spawn(World world)
        {
            var key = new Entity();
            var movement = key.AddComponent<CharacterMovement>();
            movement.Fixed = true;
            movement.IsTrigger = true;
            key.AddComponent<Key>();
            var renderChild = new Entity();
            renderChild.Transform.SetParent(key.Transform);
            var sprite = renderChild.AddComponent<SpriteRenderer>();
            renderChild.AddComponent<HoverAnimation>();
            sprite.Texture = world.ResourceManager.Get<Texture>("keyTexture");
            world.AddEntity(renderChild);
            world.AddEntity(key);

            key.Transform.GlobalPosition = Position;

            return new[] { key, renderChild };
        }
    }
}
﻿using Nurose.Core.Graphics;
using Nurose.Core.System;

namespace GMTK
{
    public sealed class PlayerInstructions : EntityInstructions
    {
        public PlayerInstructions() : base("Player")
        {
        }

        public override Entity[] Spawn(World world)
        {
            var player = new Entity();
            player.AddComponent<CharacterMovement>();
            player.AddComponent<ShootComponent>();
            player.AddComponent<FourTextureRenderer>().Texture = world.ResourceManager.Get<Texture>("playerTexture");
            player.AddComponent<Player>();
            world.AddEntity(player);

            var playerMoveIndicator = new Entity();
            playerMoveIndicator.Transform.SetParent(player.Transform);
            playerMoveIndicator.AddComponent<MoveIndicator>();
            world.AddEntity(playerMoveIndicator);

            player.Transform.GlobalPosition = Position;

            return new[] { player, playerMoveIndicator };
        }
    }
}
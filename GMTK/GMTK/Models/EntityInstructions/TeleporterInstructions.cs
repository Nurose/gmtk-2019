﻿using Nurose.Core.Graphics;
using Nurose.Core.System;
using Nurose.Core.Types;

namespace GMTK
{
    public sealed class TeleporterInstructions : EntityInstructions
    {
        private int TeleId;

        public TeleporterInstructions(int teleId) : base("Teleporter")
        {
            TeleId = teleId;
        }

        public override Entity[] Spawn(World world)
        {
            var teleporter = new Entity();
            var move = teleporter.AddComponent<CharacterMovement>();
            move.Fixed = true;
            move.IsTrigger = true;
            move.DestroyOnTileHit = false;
            move.RealGridPos = Position;
            teleporter.AddComponent<Teleporter>().TeleId = TeleId;
            var sr = teleporter.AddComponent<SpriteRenderer>();
            
            switch (TeleId)
            {
                case 0: sr.Color = Color.Green; break;
                case 1: sr.Color = Color.Orange; break;
                case 2: sr.Color = Color.Blue; break;
            }
            sr.Transform.Depth = 2;
            sr.Texture = world.ResourceManager["teleporterTexture"] as Texture;
            
            teleporter.Transform.GlobalPosition = Position;
            world.AddEntity(teleporter);

            return new[] { teleporter };
        }
    }
}
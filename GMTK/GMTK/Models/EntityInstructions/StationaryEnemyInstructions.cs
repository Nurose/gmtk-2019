﻿using System;
using Nurose.Core.Graphics;
using Nurose.Core.System;
using Nurose.Core.Types;

namespace GMTK
{
    public class StationaryEnemyInstructions : EntityInstructions
    {
        Vector2 Direction;
        public StationaryEnemyInstructions(Vector2 Direction) : base("Stationary enemy")
        {
            this.Direction = Direction;
        }

        public override Entity[] Spawn(World world)
        {
            var stationaryEnemy = new Entity();
            var shooter = stationaryEnemy.AddComponent<StationaryShooterEnemy>();

            var movement =stationaryEnemy.AddComponent<CharacterMovement>();
            movement.Direction = Direction;
            movement.Fixed = true;
            shooter.Direction = Direction;
            var t = stationaryEnemy.AddComponent<FourTextureRenderer>();
            t.Texture = world.ResourceManager.Get<Texture>("enemyTexture");
            stationaryEnemy.AddComponent<ShootComponent>();
            world.AddEntity(stationaryEnemy);
            stationaryEnemy.Transform.GlobalPosition = Position;

            t.ChangeDirection(Direction);

            return new[] { stationaryEnemy };
        }
    }
}
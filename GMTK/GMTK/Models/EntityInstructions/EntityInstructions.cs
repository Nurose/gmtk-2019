﻿using Nurose.Core.System;
using Nurose.Core.Types;

namespace GMTK
{
    public abstract class EntityInstructions
    {
        public string Name { get; set; }
        public Vector2 Position { get; set; }

        public abstract Entity[] Spawn(World world);

        public EntityInstructions Clone()
        {
            return MemberwiseClone() as EntityInstructions;
        }

        public static readonly EntityInstructions[] Instructions = {
            null, //niks spawnen
            new PlayerInstructions(),
            new KeyInstructions(),
            new StationaryEnemyInstructions(Vector2.Down),
            new StationaryEnemyInstructions(Vector2.Up),
            new StationaryEnemyInstructions(Vector2.Left),
            new StationaryEnemyInstructions(Vector2.Right),
            new TeleporterInstructions(0),
            new TeleporterInstructions(1),
            new TeleporterInstructions(2)
        };

        protected EntityInstructions(string name)
        {
            Name = name;
        }
    }
}
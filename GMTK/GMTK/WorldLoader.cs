﻿using Nurose.Core.Graphics;
using Nurose.Core.System;
using Nurose.Core.Types;
using Nurose.Core.Window;

namespace GMTK
{
    internal class WorldLoader
    {
        public static void Load(World world)
        {
            NuroseMain.Window.VSync = false;

            world.ResourceManager.TryHold("tilesheet", new Texture("Resources//tilesheet.png"));
            world.ResourceManager.TryHold("playerTexture", new Texture("Resources//player.png"));
            world.ResourceManager.TryHold("enemyTexture", new Texture("Resources//stationaryEnemy.png"));
            world.ResourceManager.TryHold("whiteTexture", new Texture("Resources//white.png"));

            world.ResourceManager.TryHold("bulletTexture", new Texture("Resources//bullet.png"));
            world.ResourceManager.TryHold("keyTexture", new Texture("Resources//key.png"));
            world.ResourceManager.TryHold("teleporterTexture", new Texture("Resources//teleporter.png"));
            world.ResourceManager.TryHold("endScreenTexture", new Texture("Resources//endScreen.png"));

            world.AddSystem<ActionSystem>();
            world.AddSystem<LevelLoadingSystem>();
            world.AddSystem<CollisionSystem>();

            var camera = new Entity();
            camera.AddComponent<Camera>();
            camera.AddComponent<DontDestroyOnLevelSwitch>();
            camera.Transform.LocalSize = new Vector2(16 * 3, 1);
            world.AddEntity(camera);

            var tiles = new Entity();
            tiles.AddComponent<TileRendererComponent>();
            tiles.AddComponent<DontDestroyOnLevelSwitch>();
            world.AddEntity(tiles);

            world.RenderTarget.SetShader(new ShaderProgram(FragmentShader.Default, VertexShader.Default));
        }
    }
}

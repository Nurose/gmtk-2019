﻿using Nurose.Core.Types;

namespace GMTK
{
    public class TurnAction : Action
    {
        public Vector2 Direction;

        public TurnAction(Vector2 direction)
        {
            Direction = direction;
        }
    }
}

﻿using Nurose.Core.System;
using Nurose.Core.Types;
using Nurose.Core.Utilities;
using Nurose.Core.Window;
using System;
using System.Collections.Generic;

namespace GMTK
{
    public class ActionSystem : ComponentSystem
    {
        private List<Action> actions;
        private int currentAction;
        public event EventHandler<Action> OnSwitchEvent;
        public event EventHandler<Action> OnStartLongPress;
        public event EventHandler<Action> OnStopLongPress;

        public float TimeHeldDownForLongPress = .34f;

        public float TimeScale;
        private float TimeBetweenSteps = .5f;
        private float TimeSinceLastStep;
        private float timeheldDown;
        public bool LongPressing { get; private set; }
        public void Start()
        {
            actions = new List<Action>
            {
                new TurnAction(Vector2.Right),
                new TurnAction(Vector2.Down),
                new TurnAction(Vector2.Left),
                new TurnAction(Vector2.Up),
                //new ShootAction()
            };
        }

        public void Update()
        {
            if (Input.GetKeyHeld(Nurose.Core.Window.Key.Space) || Input.GetButtonHeld(Button.Left))
            {
                timeheldDown += Time.DeltaTime;
                if (timeheldDown >= TimeHeldDownForLongPress)
                {
                    if (!LongPressing)
                    {
                        StartLongPress();
                    }
                    LongPress();
                    LongPressing = true;
                }
            }
            if (Input.GetKeyReleased(Nurose.Core.Window.Key.Space) || Input.GetButtonReleased(Button.Left))
            {
                if (timeheldDown >= TimeHeldDownForLongPress)
                {
                    StopLongPress();
                }
                else
                {
                    SwitchAction();
                }
                timeheldDown = 0;
            }

            TimeScale = Utils.Lerp(TimeScale, LongPressing ? 1 : 0, 1);
        }

        private void StartLongPress()
        {
            OnStartLongPress?.Invoke(this, actions[currentAction]);
            TimeSinceLastStep = 10;
        }

        private void StopLongPress()
        {
           // Logger.LogDebug("StopLongPress", this);
            //TimeScale = 0;
            LongPressing = false;
        }

        private void LongPress()
        {
            TimeSinceLastStep += Time.DeltaTime;
            if (TimeSinceLastStep >= TimeBetweenSteps)
            {
                World.SendMessage("Step");
                World.SendMessage("LateStep");
                TimeSinceLastStep = 0;
            }
            //Logger.LogDebug("LongPress", this);
            // TimeScale = 1;
        }

        private void SwitchAction()
        {
            //Logger.LogDebug("SwitchAction", this);
            if (currentAction == actions.Count - 1)
            {
                currentAction = -1;
            }
            var newAction = actions[++currentAction];
            
            OnSwitchEvent.Invoke(this, newAction);
        }
    }
}

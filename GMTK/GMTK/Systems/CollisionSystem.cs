﻿using Nurose.Core.System;
using Nurose.Core.Types;
using System;
using System.Collections.Generic;

namespace GMTK
{
    public class CollisionSystem : ComponentSystem {

        public void LateStep()
        {
            World.GetAllComponentsOfType<CharacterMovement>();
            var found = new Dictionary<Vector2, CharacterMovement>();
            foreach (var item in World.GetAllComponentsOfType<CharacterMovement>())
            {
                Vector2 key = new Vector2((int)Math.Round(item.RealGridPos.X), (int)Math.Round(item.RealGridPos.Y));
                if (found.ContainsKey(key))
                {
                    Collide(found[key], item);
                }
                else
                {
                    found.Add(item.RealGridPos, item);
                }
            }
        }

        private Level GetLevel()
        {
            return World.GetSystem<LevelLoadingSystem>().CurrentLevel;
        }


        public bool CanMove(Vector2 position)
        {
            Level level = GetLevel();
            var p = level.WorldToGrid(position);
            if(p.X<0 || p.X>= level.Width || p.Y <0 || p.Y >= level.Height-1)
            {
                return false;
            }
            return !level.Tiles[(int)p.X, (int)p.Y].Collides;
        }

        private void Collide(CharacterMovement a, CharacterMovement b)
        {
            foreach (var collide in a.Entity.GetComponents<ICollide>())
            {
                collide.OnCollide(b);
            }

            foreach (var collide in b.Entity?.GetComponents<ICollide>())
            {
                collide.OnCollide(a);
            }
        }
    }
}

﻿using Nurose.Core.Graphics;
using Nurose.Core.System;
using Nurose.Core.Types;
using Nurose.Core.Window;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace GMTK
{
    public class LevelLoadingSystem : ComponentSystem
    {
        public static readonly string[] LevelPaths = {
            "Resources\\level1.png",
            "Resources\\level2.png",
            "Resources\\level3.png"
        };

        public static readonly Tuple<int, int> FixedLevelSize = new Tuple<int, int>(16, 16);

        public List<Level> Levels { get; private set; } = new List<Level>();

        private int currentLevelindex = 0;
        private bool levelsLoaded = false;
        private List<Entity> entitiesCreated = new List<Entity>();
        public event EventHandler<Level> OnLevelSwitch;

        private TileRendererComponent tileRendererComponent;

        public Level CurrentLevel
        {
            get
            {
                tileRendererComponent = World.GetAllComponentsOfType<TileRendererComponent>()[0];

                LoadLevels();
                return Levels[currentLevelindex];
            }
        }

        public void Start()
        {
            NuroseMain.Window.Size = new Vector2(FixedLevelSize.Item1 * 3 * TileRendererComponent.TextureUnitSize, FixedLevelSize.Item2 * 3 * TileRendererComponent.TextureUnitSize);
            NuroseMain.Window.Resizable = false;
            NuroseMain.Window.CenterWindow();

            LoadLevels();
            ResetAndCreateEntities();
        }

        public void NextLevel()
        {
            currentLevelindex++;
            if (currentLevelindex >= Levels.Count)
            {
                var endscreen = new Entity();
                var sr = endscreen.AddComponent<SpriteRenderer>();
                sr.Texture = World.ResourceManager["endScreenTexture"] as Texture;
                endscreen.Transform.LocalSize = new Vector2(16, 16);
                endscreen.Transform.GlobalPosition = new Vector2(-8, -8);
                World.AddEntity(endscreen);
                currentLevelindex = 0;
                return;
            }

            OnLevelSwitch?.Invoke(this, CurrentLevel);
            ResetAndCreateEntities();
        }

        public void PreviousLevel()
        {
            currentLevelindex--;
            if (currentLevelindex < 0)
            {
                currentLevelindex = 0;
            }

            OnLevelSwitch?.Invoke(this, CurrentLevel);
            ResetAndCreateEntities();
        }

        public void ReloadLevel()
        {
            OnLevelSwitch?.Invoke(this, CurrentLevel);
            ResetAndCreateEntities();
        }

        private void ResetAndCreateEntities()
        {
            Reset();

            foreach (var instructions in CurrentLevel.Entities)
            {
                entitiesCreated.AddRange(instructions.Spawn(World));
            }
        }

        private void Reset()
        {
            foreach (var transform in World.GetAllComponentsOfType<Transform>())
            {
                if (transform.Entity.GetComponents<DontDestroyOnLevelSwitch>().Count == 0)
                {
                    transform.Entity.Destroy();
                }
            }

            foreach (var transform in World.GetNotYetCreatedComponentsOfType<Transform>())
            {
                if (transform.Entity.GetComponents<DontDestroyOnLevelSwitch>().Count == 0)
                {
                    transform.Entity.Destroy();
                }
            }

            entitiesCreated.Clear();
        }

        private void LoadLevels()
        {
            if (levelsLoaded)
            {
                return;
            }

            Levels.Clear();

            foreach (string path in LevelPaths)
            {
                if (!LoadLevel(path, out var tiles, out var entities))
                {
                    continue;
                }

                Levels.Add(new Level(FixedLevelSize.Item1, FixedLevelSize.Item2, tiles, entities, tileRendererComponent));
            }

            levelsLoaded = true;
        }

        private bool LoadLevel(string path, out TileType[,] tiles, out EntityInstructions[] entities)
        {
            tiles = new TileType[FixedLevelSize.Item1, FixedLevelSize.Item2];
            entities = new EntityInstructions[0];

            var image = Image.FromFile(path, false) as Bitmap;

            if (image.Width != FixedLevelSize.Item1 || image.Height != FixedLevelSize.Item2)
            {
                Logger.LogWarning($"{path} is not a valid level file: Size needs to be ({FixedLevelSize.Item1}, {FixedLevelSize.Item2})");
                return false;
            }

            var dynamicEntities = new List<EntityInstructions>();

            for (int y = 0; y < image.Height; y++)
            {
                for (int x = 0; x < image.Width; x++)
                {
                    var pixel = image.GetPixel(x, y);

                    byte tile = pixel.R;
                    byte entity = pixel.G;

                    if (tile > TileType.Types.Length)
                    {
                        Logger.LogWarning($"{path} is not a valid level file: Data at ({x}, {y}) is out of range");
                        return false;
                    }

                    if (entity > EntityInstructions.Instructions.Length)
                    {
                        Logger.LogWarning($"{path} is not a valid level file: Data at ({x}, {y}) is out of range");
                        return false;
                    }

                    tiles[x, y] = TileType.Types[tile];
                    if (entity != 0)
                    {
                        var instructions = EntityInstructions.Instructions[entity].Clone();
                        instructions.Position = Level.GridToWorld(x, y, FixedLevelSize.Item1, FixedLevelSize.Item2);
                        Logger.LogDebug($"{x},{y} -> {instructions.Position.X},{instructions.Position.Y}", this);
                        dynamicEntities.Add(instructions);
                    }
                }
            }

            entities = dynamicEntities.ToArray();

            image.Dispose();

            return true;
        }
    }
}

﻿using Nurose.Core.Graphics;
using Nurose.Core.Types;

namespace GMTK
{
    public class GridTask : IRendererTask
    {
        private VertexBuffer vertexBuffer;
        private Texture texture;
        private readonly int textureUnitSize;
        public Level Level;

        public GridTask(Level level, Texture texture, int TextureUnitSize)
        {
            this.Level = level;
            this.texture = texture;
            textureUnitSize = TextureUnitSize;
            Refresh();
        }

        public void Refresh()
        {
            Vertex[] vertices = new Vertex[Level.Height * Level.Width * 4];
            int i = 0;
            float xSize = textureUnitSize / texture.Size.X;
            float ySize = textureUnitSize / texture.Size.Y;
            for (int x = 0; x < Level.Width; x++)
            {
                for (int y = 0; y < Level.Height; y++)
                {
                    var basePos = Level.Tiles[x, y].GetUV(textureUnitSize, texture.Size);

                    vertices[i] = new Vertex(new Vector2(x, y), Color.White, basePos);
                    vertices[i + 1] = new Vertex(new Vector2(x + 1, y), Color.White, basePos + new Vector2(xSize, 0));
                    vertices[i + 2] = new Vertex(new Vector2(x + 1, y + 1), Color.White, basePos + new Vector2(xSize, ySize));
                    vertices[i + 3] = new Vertex(new Vector2(x, y + 1), Color.White, basePos + new Vector2(0, ySize));
                    i += 4;
                }
            }
            vertexBuffer = new VertexBuffer(vertices);
        }

        public void Execute(IRenderTarget RenderTarget)
        {
            RenderTarget.SetTexture(texture);
            RenderTarget.Draw(vertexBuffer, PrimitiveType.Quads);
        }
    }
}

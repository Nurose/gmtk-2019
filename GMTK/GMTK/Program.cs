﻿using Nurose.Core.Window;
using System;
using Nurose.OpenTK;

namespace GMTK
{
    class Program
    {
        static void Main(string[] args)
        {
            NuroseMain.SetGraphicsFactory<OpenTKGraphicsFactory>();
            NuroseMain.SetWindowFactory<OpenTKWindowFactory>();
            NuroseMain.Create("Step", 1280, 768, 60);
            WorldLoader.Load(NuroseMain.Instance.World);
            NuroseMain.Window.CenterWindow();
            NuroseMain.Instance.Start();

        }
    }
}
